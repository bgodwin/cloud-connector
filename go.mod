module brandongodwin.net/cloud-connector

go 1.16

require (
	github.com/cenkalti/backoff/v4 v4.1.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/rancher/remotedialer v0.2.5
	github.com/sirupsen/logrus v1.4.2
)
