# Authentication

Authentication to the proxy side in designed to be flexible and integrate easily into an existing environment. It allows complex systems with dedicated microservices to handle authentication and authorization and issuing tokens, or simply a single system that creates and signs its own tokens.

Authentication is handled by:

* For the data plane, JWT token passed by basic username/password
    * Must have time constraint on it
    * Must have subject, used in audit logs
    * Must have audience of "data plane"
    * Must have custom attribute listing the tunnel ID that this token is for
* For the control plane, JWT bearer token with:
    * time constraint
    * subject
    * audience of "control plane"
* For the tunnel endpoint
    * Mutual TLS auth, with public key preregistered and associated with a tunnel ID