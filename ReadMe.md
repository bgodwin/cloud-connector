# Cloud Connector

## Known Limitations

* [cURL may drop proxy auth password if the JWT is longer than 255 characters.](https://github.com/curl/curl/issues/5448)
