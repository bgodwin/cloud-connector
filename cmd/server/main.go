package main

import (
	"crypto/rsa"
	"crypto/sha1"
	"crypto/tls"
	"crypto/x509"
	"encoding/hex"
	"encoding/base64"
	"errors"
	"flag"
	"io"
	"net/http"
	"time"
	"strings"

	"github.com/gorilla/mux"
	"github.com/rancher/remotedialer"
	"github.com/sirupsen/logrus"
	jwt "github.com/dgrijalva/jwt-go"
)

type TunnelJWTClaims struct {
	TunnelID string `json:"tunnel_id"`
	jwt.StandardClaims
}

func authorizer(req *http.Request) (string, bool, error) {
	if len(req.TLS.PeerCertificates) != 1 {
		return "", false, errors.New("incorrect number of peer certificates sent, expecting 1")
	}

	// TODO: Check cast valid
	publicKey := req.TLS.PeerCertificates[0].PublicKey.(*rsa.PublicKey)
	pubKeyFingerprint := sha1.Sum(x509.MarshalPKCS1PublicKey(publicKey))
	pubKeyFingerprintHex := hex.EncodeToString(pubKeyFingerprint[:])
	logrus.Info(pubKeyFingerprintHex)

	if pubKeyFingerprintHex == "a7f5d757985ba1044ef0a7f676a535df39ba8ddd" {
		// TODO: Lookup the tunnel ID for this client
		return "foo", true, nil
	} else {
		return "", false, errors.New("Unknown client")
	}
}

var (
	JWT_PUBLIC_KEY interface{}
)

func handleTunneling(server *remotedialer.Server, w http.ResponseWriter, r *http.Request) {
	// Validate JWT auth to use the proxy
	username, tokenString, _ := parseBasicAuth(r.Header.Get("Proxy-Authorization"))
	if username != "jwt" {
		http.Error(w, "JWT authentication not provided", http.StatusUnauthorized)
		return
	}
	token, err := jwt.ParseWithClaims(tokenString, &TunnelJWTClaims{}, func(token *jwt.Token) (interface{}, error) {
		return JWT_PUBLIC_KEY, nil
	})

	var tunnelID string
	if claims, ok := token.Claims.(*TunnelJWTClaims); ok && token.Valid {
		// One additional validation, let's make sure the token isn't super long lived, to prevent insecure token usage
		if claims.StandardClaims.ExpiresAt - claims.StandardClaims.IssuedAt > 72*60*60 {
			http.Error(w, "Token validity period too large, issue tokens valid for less than 72 hours", http.StatusUnauthorized)
			return
		}

		tunnelID = claims.TunnelID
	} else {
		logrus.Error(err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	// TODO: remove hard coded tunnel ID
	tunnelID = "foo"
	dest_conn, err := server.Dialer(tunnelID, 15*time.Second)("tcp", r.Host)
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		return
	}
	w.WriteHeader(http.StatusOK)
	hijacker, ok := w.(http.Hijacker)
	if !ok {
		http.Error(w, "Hijacking not supported", http.StatusInternalServerError)
		return
	}
	client_conn, _, err := hijacker.Hijack()
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
	}
	go transfer(dest_conn, client_conn)
	go transfer(client_conn, dest_conn)
}

func transfer(destination io.WriteCloser, source io.ReadCloser) {
	defer destination.Close()
	defer source.Close()
	io.Copy(destination, source)
}

// Copied from Go stdlib
func parseBasicAuth(auth string) (username, password string, ok bool) {
	const prefix = "Basic "

	// Case insensitive prefix match. See Issue 22736.
	if len(auth) < len(prefix) || !strings.EqualFold(auth[:len(prefix)], prefix) {
		return
	}

	c, err := base64.StdEncoding.DecodeString(auth[len(prefix):])
	if err != nil {
		return
	}

	cs := string(c)
	s := strings.IndexByte(cs, ':')
	if s < 0 {
		return
	}

	return cs[:s], cs[s+1:], true
}

func main() {
	addr := flag.String("listen", "0.0.0.0:8443", "Listen address")
	jwt_pub_key := flag.String("jwt-pub", "", "Public key for validating JWTs")
	certPath := flag.String("cert", "", "Path to the server certificate chain")
	keyPath := flag.String("key", "", "Path to the private key")
	flag.Parse()

	KEY_BYTES, _ := base64.StdEncoding.DecodeString(*jwt_pub_key)
	JWT_PUBLIC_KEY, _ = x509.ParsePKIXPublicKey(KEY_BYTES)

	handler := remotedialer.New(authorizer, remotedialer.DefaultErrorWriter)

	router := mux.NewRouter()
	router.Handle("/connect", handler)

	logrus.Info("Listening on ", *addr)
	server := http.Server{
		Addr: *addr,
		TLSConfig: &tls.Config{
			MinVersion:               tls.VersionTLS12,
			CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
			PreferServerCipherSuites: true,
			CipherSuites: []uint16{
				tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
				tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
				tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_RSA_WITH_AES_256_CBC_SHA,
			},
			ClientAuth: tls.RequestClientCert,
			// VerifyPeerCertificate: func(rawCerts [][]byte, verifiedChains [][]*x509.Certificate) error {
			// 	clientCert, err := x509.ParseCertificate(rawCerts[0])
			// 	if err != nil {
			// 		return err
			// 	}

			// 	// Verify that this is a known client by their public key
			// 	// TODO: Check cast valid
			// 	publicKey := clientCert.PublicKey.(*rsa.PublicKey)
			// 	pubKeyFingerprint := sha1.Sum(x509.MarshalPKCS1PublicKey(publicKey))
			// 	if hex.EncodeToString(pubKeyFingerprint[:]) != "a7f5d757985ba1044ef0a7f676a535df39ba8ddd" {
			// 		return errors.New("Unknown client")
			// 	}

			// 	return nil
			// },
		},
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method == http.MethodConnect {
				handleTunneling(handler, w, r)
			} else {
				router.ServeHTTP(w, r)
			}
		}),
	}
	if err := server.ListenAndServeTLS(*certPath, *keyPath); err != nil {
		logrus.Fatal(err)
	}
}
