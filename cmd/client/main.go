package main

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"

	"brandongodwin.net/cloud-connector/internal/client"
)

func main() {
	server := flag.String("server", "", "Address to connect to (eg: tunnel.example.com)")
	certPath := flag.String("cert", "", "Path to the client certificate for authentication")
	keyPath := flag.String("key", "", "Path to the private key used for authentication")
	flag.Parse()

	// Wire up the signal handling so that we can gracefully shutdown
	ctx, cancel := context.WithCancel(context.Background())
	signalChan := make(chan os.Signal)
	signal.Notify(signalChan, syscall.SIGTERM, syscall.SIGINT)

	go func() {
		defer signal.Stop(signalChan)
		<-signalChan
		cancel()
	}()

	config := &client.Config{
		TunnelService: *server,
		CertificatePath: *certPath,
		PrivateKeyPath: *keyPath,
	}
	client := client.New(ctx, config)
	if err := client.Start(); err != nil {
		log.Error(err)
		os.Exit(1)
	}
}
