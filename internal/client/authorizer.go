package client

import (
	"github.com/sirupsen/logrus"
)

var allowlist = map[string]bool{
	"ipinfo.io:443": true,
}

// AllowlistAuthorizer determines whether a connection is allowed to be proxied to the requested host or not
func allowlistAuthorizer(proto, address string) bool {
	// if allowlist[address] {
	// 	return true
	// } else {
	// 	return false
	// }
	logrus.WithField("address", address).Info("Connect request")
	return true
}
