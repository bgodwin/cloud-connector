package client

// Config is the tunnel client configuration
type Config struct {
	TunnelService string
	CertificatePath string
	PrivateKeyPath string
}
