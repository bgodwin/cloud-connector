package client

import (
	"context"
	"crypto/tls"
	"errors"
	"net/http"
	"net/url"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/gorilla/websocket"
	"github.com/rancher/remotedialer"
	log "github.com/sirupsen/logrus"
)

// Client is the tunnel client, responsible for maintaining multiple tunnels connections and proxying connections
type Client struct {
	ctx    context.Context
	config *Config
}

// New creates a new instance of the tunnel client
func New(ctx context.Context, config *Config) *Client {
	return &Client{
		ctx:    ctx,
		config: config,
	}
}

// Start is a blocking function that begins the Client's functionality (establishing tunnels, proxying connections, etc)
func (client *Client) Start() error {
	// Adjust the URL for our tunnel service to a websocket URL
	wsURL := *&url.URL{
		Scheme: "wss",
		Host:   client.config.TunnelService,
		Path:   "/connect",
	}

	cer, err := tls.LoadX509KeyPair(client.config.CertificatePath, client.config.PrivateKeyPath)
	if err != nil {
		return err
	}

	backoffConfig := backoff.NewExponentialBackOff()
	// retry forever
	backoffConfig.MaxElapsedTime = 0
	err = backoff.Retry(
		func() error {
			remotedialer.ClientConnect(
				client.ctx,
				wsURL.String(),
				http.Header{},
				&websocket.Dialer{
					Proxy:            http.ProxyFromEnvironment,
					HandshakeTimeout: 10 * time.Second,
					TLSClientConfig: &tls.Config{
						Certificates: []tls.Certificate{cer},
					},
				},
				allowlistAuthorizer,
				func(c context.Context) error {
					log.Info("onConnect called")
					return nil
				},
			)

			// If we're here, it means the client failed and returned
			return errors.New("Client unable to connect or disconnected")
		},
		backoff.WithContext(backoffConfig, client.ctx),
	)
	if err != nil {
		return err
	}

	return nil
}
